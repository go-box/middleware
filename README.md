Go-Box Middleware
=================

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-box/middleware)](https://goreportcard.com/report/gitlab.com/go-box/middleware)

Various middleware for use with the Chi web framework.

List of Middleware
==================

go-box/middleware/static
------------------------

A middleware for serving static files, allowing static files to be served at
`/` rather than a sub route like `/static`. This is useful when building
single page apps (SPAs).

The design of this middleware is very much inspired by
https://github.com/gin-contrib/static but for use with Chi instead.

Directory indexes are permanently turned off because it tends to interfere
with routes at the same path, and directory indexes are not that great for
security anyway.

Usage:

```go
import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/go-box/middleware/static"
)

func main() {
	r := chi.NewRouter()
	r.Use(static.Serve("/", "public"))
	http.ListenAndServe(":8000", r)
}
```

GoDoc
-----

https://pkg.go.dev/gitlab.com/go-box/middleware
