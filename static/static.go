package static

import (
	"net/http"
	"os"
	"path"
	"strings"
)

const INDEX = "index.html"

type ServeFileSystem interface {
	http.FileSystem
	Exists(prefix string, path string) bool
}

type localFileSystem struct {
	http.FileSystem
	root string
}

func LocalFile(root string) *localFileSystem {
	return &localFileSystem{
		FileSystem: http.Dir(root),
		root:       root,
	}
}

func (l *localFileSystem) Exists(prefix string, filepath string) bool {
	if p := strings.TrimPrefix(filepath, prefix); len(p) < len(filepath) {
		name := path.Join(l.root, p)
		stats, err := os.Stat(name)
		if err != nil {
			return false
		}

		// Completely disable directory listing as it interferes with routes.
		// Unless there is an index.html file here in which case we allow it.
		if stats.IsDir() {
			index := path.Join(name, INDEX)
			_, err := os.Stat(index)
			if err != nil {
				return false
			}
		}
		return true
	}
	return false
}

// Serve returns a middleware handler that serves static files in the given directory.
func Serve(urlPrefix string, root string) func(next http.Handler) http.Handler {
	// LocalFile was used by the original library, but we just pass a string instead.
	// This is because we don't have an option for listing directory indexes at all,
	// so a simple string is fine in that case, and it makes it easier for the caller.
	fs := LocalFile(root)

	fileServer := http.FileServer(fs)
	if urlPrefix != "" {
		fileServer = http.StripPrefix(urlPrefix, fileServer)
	}

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if fs.Exists(urlPrefix, r.URL.Path) {
				fileServer.ServeHTTP(w, r)
			} else {
				next.ServeHTTP(w, r)
			}
		})
	}
}
